# README #

This README covers steps which are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

      A solution to AGL Programming Challenge (http://agl-developer-test.azurewebsites.net/)

* Version 2.0 (re-written from the scratch in .Net Core 2.0)


### How do I get set up? ###

* Summary of set up (Required VS 2017 with .Net Core 2.0 components)
* Dependencies (Please see Package file, nuget should resolve all dependencies)
* How to run tests (User xUnit test runner explorer)
* Run/Debug from VS Studio as it's a Console application


### Who do I talk to? ###

* Repo owner or admin (Please send email to dr_maq@hotmail.com for any further improvements)