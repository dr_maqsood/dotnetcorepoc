using System.Linq;
using Xunit;

namespace DotNetCorePoC.Tests
{
    public class UnitTest
    {
        [Fact]
        public async void ShouldGetPeopleData()
        {
            OwnerReader reader = new OwnerReader("http://agl-developer-test.azurewebsites.net/people.json");
            var genderList = await reader.GetCatsByGender();
            Assert.NotNull(genderList);
        }
        [Fact]
        public async void ShouldNotBeAnyCat()
        {
            OwnerReader reader = new OwnerReader("http://agl-developer-test.azurewebsites.net/people.json");
            var genderList = await reader.GetCatsByGender();
            var nonCatList = genderList.ToList().Where(x => x.Owner.Any(y => y.Pets != null && y.Pets.Any(z => z.Type != "Cat"))).ToList();
            Assert.True(nonCatList.Count == 0);
        }
    }
}