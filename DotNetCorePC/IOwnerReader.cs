﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DotNetCorePoC.DTO;

namespace DotNetCorePoC
{
    public interface IOwnerReader
    {
        /// <summary>
        /// Get cats by gender
        /// </summary>
        /// <returns>return list of cats by gender</returns>
        Task<List<OwnerGenderGroup>> GetCatsByGender();
    }
}
