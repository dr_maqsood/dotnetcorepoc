﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DotNetCorePoC.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DotNetCorePoC
{
    /// <summary>
    /// Owner Reader class
    /// </summary>
    public class OwnerReader : IOwnerReader
    {
        private readonly string _url;

        public OwnerReader(string url)
        {
            _url = url;
        }
        /// <summary>
        /// Get cats by gender
        /// </summary>
        /// <returns>return list of cats by gender</returns>
        public async Task<List<OwnerGenderGroup>> GetCatsByGender()
        {
            List<OwnerGenderGroup> ownerByGenderList = new List<OwnerGenderGroup>();

            using (var client = new HttpClient())
            {
                Console.WriteLine($"1) Receving people data from web api... {Environment.NewLine}");
                using (HttpResponseMessage response = await client.GetAsync(_url))
                {
                    response.EnsureSuccessStatusCode();

                    if (response.IsSuccessStatusCode)
                    {
                        var peopleDataResult = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine($"2) Received people data from web api as below {Environment.NewLine}{peopleDataResult}");
                        var ownerList = JsonConvert.DeserializeObject<List<Owner>>(peopleDataResult);

                        Console.WriteLine($"{Environment.NewLine}3) Presenting People Objects: {JToken.Parse(JsonConvert.SerializeObject(ownerList)).ToString(Formatting.Indented)}");
                        
                        var ownerListByGender = ownerList.ToList()
                            .GroupBy(z => z.Gender)
                            .Select(k => new OwnerGenderGroup
                            {
                                Gender = k.Key,
                                Owner = k.Select(i => new Owner
                                {
                                    Name = i.Name,
                                    Age = i.Age,
                                    Gender = i.Gender,
                                    Pets = i.Pets?.Where(x => x.Type == "Cat").ToList(),
                                }).ToList()
                            })
                            .ToList();
                        Console.WriteLine($"{Environment.NewLine}4) Grouped by Gender {Environment.NewLine}");
                        ownerByGenderList = ownerListByGender;
                    }
                }
                return ownerByGenderList;
            }
        }
    }
}
