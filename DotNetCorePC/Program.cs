﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCorePoC
{
    class Program
    {
        private static readonly string WebServiceUrl = "http://agl-developer-test.azurewebsites.net/people.json";

        public static async Task Main(string[] args)
        {
            //OwnerReader reader = new OwnerReader(Settings.Default.WebserviceUrl);
            OwnerReader reader = new OwnerReader(WebServiceUrl);
            
            var genderList = await reader.GetCatsByGender();

            Console.WriteLine("======\t======");
            Console.WriteLine("Gender\tPerson");
            Console.WriteLine("======\t======");

            //Display all the cats under a heading of the gender of their owner
            foreach (var gender in genderList)
            {
                //Display Gender
                Console.WriteLine(gender.Gender);
                foreach (var item in gender.Owner.OrderBy(x => x.Name))
                {
                    //Display all cats for this gender
                    Console.WriteLine("\t" + item.Name);
                }
            }
            Console.WriteLine($"{Environment.NewLine}Done! Press any key...");
            Console.ReadKey();
        }
       
    }
}