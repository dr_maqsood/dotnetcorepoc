﻿namespace DotNetCorePoC.DTO
{
    public class Pet
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
