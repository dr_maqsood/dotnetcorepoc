﻿using System.Collections.Generic;

namespace DotNetCorePoC.DTO
{
    public class OwnerGenderGroup
    {
        public string Gender { get; set; }
        public List<Owner> Owner { get; set; }
    }
}
